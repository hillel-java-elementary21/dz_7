package com.homework.contactbook.service;

import com.homework.contactbook.exception.DuplicateContactException;
import com.homework.contactbook.model.Contact;
import com.homework.contactbook.persistance.ContactRepository;
import com.homework.contactbook.service.contactservice.ContactService;
import com.homework.contactbook.utills.validator.Validator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Matchers.any;

class ContactServiceTest {
    private ContactService contactService;

    @Mock
    private ContactRepository contactRepository;
    @Mock
    private Validator<Contact> validator;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        contactService = new ContactService(contactRepository, validator);
    }

    @Test
    @DisplayName("Проверка поиска всех")
    void findAll() {
        List<Contact> contacts = new ArrayList<>();
        Mockito.when(contactRepository.findAll()).thenReturn(contacts);
        List<Contact> returned = contactService.findAll();
        assertEquals(contacts, returned);
    }

    @Test
    @DisplayName("Проверка добавления контакта")
    void saveTest() {
        Contact contact = new Contact();
        Mockito.when(contactRepository.findByValue(any()))
                .thenReturn(Optional.empty());
        Mockito.when(validator.isValid(any())).thenReturn(true);
        contactService.save(contact);
        Mockito.verify(contactRepository).save(Mockito.eq(contact));
    }

    @Test
    @DisplayName("Проверка добавления существующего контакта")
    void saveDuplicateTest() {
        Contact contact = new Contact();
        Mockito.when(contactRepository.findByValue(any()))
                .thenReturn(Optional.of(new Contact()));
        assertThrows(DuplicateContactException.class, () -> contactService.save(contact));
    }
}