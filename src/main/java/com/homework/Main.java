package com.homework;

import com.homework.contactbook.factory.ContactServiceFactory;
import com.homework.contactbook.factory.DataBaseServiceFactory;
import com.homework.contactbook.factory.InMemoryContactServiceFactory;
import com.homework.contactbook.factory.JsonFileContactServiceFactory;
import com.homework.contactbook.model.Contact;
import com.homework.contactbook.service.authservice.AuthService;
import com.homework.contactbook.service.contactservice.ContactService;
import com.homework.contactbook.ui.menu.Menu;
import com.homework.contactbook.ui.menu.item.inmemoryitem.*;
import com.homework.contactbook.ui.menu.view.*;
import com.homework.contactbook.utills.validator.Validator;
import com.homework.contactbook.utills.validator.contacts.ContactValidator;
import com.homework.contactbook.utills.validator.contacts.EmailValidator;
import com.homework.contactbook.utills.validator.contacts.PhoneValidator;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Main {

    public static final String APPLICATION_MODE_KEY = "application.mode";

    public static void main(String[] args) {
        ContactServiceFactory serviceFactory = getContactRepositoryFactory();
        ContactService contactService = serviceFactory.createContactService();
        AuthService authService = serviceFactory.createAuthService();

        Scanner scanner = new Scanner(System.in);
        ContactView contactView = new DefaultContactView(scanner);
        ErrorView errorView = new ErrorView();
        UserView userView = new NetworkUserView(scanner);
        System.out.println("*** Телефонная книга 4.0.0 ***");
        Menu menu = new Menu(List.of(
                new AddContactMenuItem(contactService, contactView, errorView, authService),
                new RegistrationUserMenuItem(userView, errorView, authService),
                new ShowAllSortedBySurnameMenuItem(contactService, contactView, authService),
                new ShowOnlyNumberMenuItem(contactService, contactView, authService),
                new ShowOnlyEmailMenuItem(contactService, contactView, authService),
                new FindByNameMenuItem(contactService, contactView, errorView, authService),
                new FindByValueStartMenuItem(contactService, contactView, errorView, authService),
                new RemoveContactMenuItem(contactService, contactView, errorView, authService),
                new AuthorizationUserMenuItem(userView, errorView, authService),
                new LogoutUserMenuItem(authService),
                new ExitMenuItem()
        ), scanner);
        menu.run();
    }

    /*
// NETWORK
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:postgresql://127.0.0.1:5432/contactsbook");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("nfpbr");
        Validator<Contact> contactValidator = getContactValidator();
        DataSource dataSource = new HikariDataSource(hikariConfig);
        NetworkServiceFactory networkServiceFactory = new NetworkServiceFactory(contactValidator);
        AuthService authService = networkServiceFactory.createAuthService();
        NetworkContactService contactService = networkServiceFactory.createContactService();
        Menu menu = new Menu(List.of(
                new AuthorizationUserNetworkMenuItem(authService, userView, errorView),
                new RegistrationUserNetworkMenuItem(authService, userView, errorView),
                new AddContactNetworkMenuItem(authService, contactService, contactView, errorView),
                new ShowAllContactNetworkMenuItem(authService, contactService, contactView),
                new FindContactByValueNetworkMenuItem(authService, contactService, contactView, errorView),
                new FindContactByNameNetworkMenuItem(authService, contactService, contactView, errorView),
                new LogoutUserNetworkMenuItem(authService),
                new ExitMenuItem()
        ), scanner);
*/
    private static ContactServiceFactory getContactRepositoryFactory() {
        Properties properties = getProperties();
        Validator<Contact> contactValidator = getContactValidator();
        String mode = properties.getProperty(APPLICATION_MODE_KEY);
        Map<String, ContactServiceFactory> factories = Map.of(
                "IN_MEMORY", new InMemoryContactServiceFactory(contactValidator),
                "JSON_FILE", new JsonFileContactServiceFactory(properties, contactValidator),
                "DATA_BASE", new DataBaseServiceFactory(properties, contactValidator)
        );
        if (!factories.containsKey(mode)) {
            throw new RuntimeException("INVALID MODE CONFIGURATION");
        }
        return factories.get(mode);
    }

    private static Validator<Contact> getContactValidator() {
        return new ContactValidator(List.of(
                new EmailValidator(),
                new PhoneValidator()
        ));
    }

    private static Properties getProperties() {
        String propertiesSource = System.getProperty("property.source");
        propertiesSource = Optional.ofNullable(propertiesSource)
                .orElse("CLASSPATH");
        Properties properties = new Properties();
        try {
            if (propertiesSource.equals("CLASSPATH")) {
                properties.load(
                        Main.class
                                .getClassLoader()
                                .getResourceAsStream("config.properties")
                );
            } else {
                properties.load(new FileReader(propertiesSource));
            }
        } catch (IOException e) {
            properties.setProperty(APPLICATION_MODE_KEY, "IN_MEMORY");
        }
        return properties;
    }
}
