package com.homework.contactbook.persistance;

import com.homework.contactbook.model.Contact;
import com.homework.contactbook.model.ContactType;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class FileContactRepository implements ContactRepository {

    private final ContactFile contactFile;

    @Override
    public List<Contact> findAll() {
        return contactFile.readAll();
    }

    @Override
    public List<Contact> findByType(ContactType type) {
        return contactFile.readAll().stream()
                .filter(c -> c.getType() == type)
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByValueStart(String valueStart) {
        return contactFile.readAll().stream()
                .filter(c -> Objects.nonNull(c.getValue()))
                .filter(c -> c.getValue().startsWith(valueStart))
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByName(String namePart) {
        return contactFile.readAll().stream()
                .filter(c -> Objects.nonNull(c.getFullName()))
                .filter(c -> c.getFullName().contains(namePart))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Contact> findByValue(String value) {
        return contactFile.readAll().stream()
                .filter(c -> Objects.nonNull(c.getValue()))
                .filter(c -> c.getValue().equals(value))
                .findFirst();
    }

    @Override
    public void deleteByValue(String value) {
        List<Contact> toSave = contactFile.readAll().stream()
                .filter(c -> !c.getValue().equals(value))
                .collect(Collectors.toList());
        contactFile.saveAll(toSave);
    }

    @Override
    public void save(Contact contact) {
        List<Contact> contacts = contactFile.readAll();
        contact.setId(UUID.randomUUID().toString());
        contacts.add(contact);
        contactFile.saveAll(contacts);
    }
}
