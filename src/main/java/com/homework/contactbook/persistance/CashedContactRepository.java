package com.homework.contactbook.persistance;

import com.homework.contactbook.model.Contact;
import com.homework.contactbook.model.ContactType;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RequiredArgsConstructor
public class CashedContactRepository implements ContactRepository {
    private final ContactRepository repository;
    private List<Contact> cacheAll;
    private Map<ContactType, List<Contact>> cacheType = new HashMap<>();

    private void invalidateCache() {
        cacheAll = null;
        cacheType.clear();
    }

    @Override
    public List<Contact> findAll() {
        if (cacheAll == null) {
            cacheAll = repository.findAll();
        }
        return cacheAll;
    }

    @Override
    public List<Contact> findByType(ContactType type) {
        if (!cacheType.containsKey(type)) {
            cacheType.put(type, repository.findByType(type));
        }
        return cacheType.get(type);
    }

    @Override
    public List<Contact> findByValueStart(String valueStart) {
        return repository.findByValueStart(valueStart);
    }

    @Override
    public List<Contact> findByName(String namePart) {
        return repository.findByName(namePart);
    }

    @Override
    public Optional<Contact> findByValue(String value) {
        return repository.findByValue(value);
    }

    @Override
    public void deleteByValue(String value) {
        repository.deleteByValue(value);
        invalidateCache();
    }

    @Override
    public void save(Contact contact) {
        repository.save(contact);
        invalidateCache();
    }
}
