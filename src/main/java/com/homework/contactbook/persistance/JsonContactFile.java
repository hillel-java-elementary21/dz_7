package com.homework.contactbook.persistance;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.homework.contactbook.model.Contact;
import lombok.RequiredArgsConstructor;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class JsonContactFile implements ContactFile {

    private final ObjectMapper objectMapper;
    private final String fileName;

    @Override
    public List<Contact> readAll() {
        try (InputStream fis = new FileInputStream(fileName)) {
            return objectMapper.readValue(fis, new TypeReference<ArrayList<Contact>>() {
            });

        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    @Override
    public void saveAll(List<Contact> contacts) {
        try (OutputStream os = new FileOutputStream(fileName)) {
            byte[] jsonByte = objectMapper.writeValueAsBytes(contacts);
            os.write(jsonByte);
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
