package com.homework.contactbook.persistance;

import com.homework.contactbook.model.Contact;
import com.homework.contactbook.model.ContactType;

import java.util.List;
import java.util.Optional;

public interface ContactRepository {
    List<Contact> findAll();

    List<Contact> findByType(ContactType type);

    List<Contact> findByValueStart(String valueStart);

    List<Contact> findByName(String namePart);

    Optional<Contact> findByValue(String value);

    void deleteByValue(String value);

    void save(Contact contact);
}
