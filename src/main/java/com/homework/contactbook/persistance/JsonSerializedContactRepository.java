package com.homework.contactbook.persistance;

import com.google.gson.Gson;
import com.homework.contactbook.model.Contact;
import com.homework.contactbook.model.ContactType;
import lombok.AllArgsConstructor;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@AllArgsConstructor
public class JsonSerializedContactRepository implements ContactRepository {
    private String path;
    private Gson gson;

    @Override
    public List<Contact> findAll() {
        return getAllContact().stream()
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByType(ContactType type) {
        return getAllContact().stream()
                .filter(c -> c.getType() == type)
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByValueStart(String valueStart) {
        return getAllContact().stream()
                .filter(c -> c.getValue().startsWith(valueStart))
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByName(String namePart) {
        return getAllContact().stream()
                .filter(c -> c.getFullName().contains(namePart))
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Contact> findByValue(String value) {
        return findAll().stream()
                .filter(c -> c.getValue().equals(value))
                .findFirst();
    }

    @Override
    public void deleteByValue(String value) {
        List<Contact> temp = findAll().stream()
                .filter(c -> !c.getValue().equals(value))
                .collect(Collectors.toList());
        rewriteJsonContactInFile(temp);
    }

    @Override
    public void save(Contact contact) {
        contact.setId(UUID.randomUUID().toString());
        writeJsonContactInFile(contact);
    }

    private void writeJsonContactInFile(Contact contact) {
        try (PrintWriter writer = new PrintWriter(new FileWriter(path, true))) {
            writer.printf("%s\n", gson.toJson(contact));
            writer.flush();
        } catch (IOException exception) {
        }
    }

    private void rewriteJsonContactInFile(List<Contact> contacts) {
        try (PrintWriter writer = new PrintWriter(new FileWriter(path))) {
            for (Contact c : contacts) {
                writer.printf("%s\n", gson.toJson(c));
                writer.flush();
            }
        } catch (IOException exception) {
        }
    }

    private List<Contact> getAllContact() {
        List<Contact> contacts = new ArrayList<>();
        try (BufferedReader r = new BufferedReader(new FileReader(path))) {
            while (true) {
                String line = r.readLine();
                if (line == null) break;
                contacts.add(gson.fromJson(line, Contact.class));
            }
        } catch (IOException | IndexOutOfBoundsException e) {
        }
        return contacts;
    }
}
