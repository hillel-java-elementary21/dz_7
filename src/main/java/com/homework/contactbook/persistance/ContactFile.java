package com.homework.contactbook.persistance;

import com.homework.contactbook.model.Contact;

import java.util.List;

public interface ContactFile {

    List<Contact> readAll();

    void saveAll(List<Contact> contacts);
}
