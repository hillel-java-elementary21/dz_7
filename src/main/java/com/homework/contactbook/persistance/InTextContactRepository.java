package com.homework.contactbook.persistance;

import com.homework.contactbook.model.Contact;
import com.homework.contactbook.model.ContactType;
import lombok.AllArgsConstructor;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@AllArgsConstructor
public class InTextContactRepository implements ContactRepository {
    private String path;

    @Override
    public List<Contact> findAll() {
        return getAllContact().stream()
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByType(ContactType type) {
        return getAllContact().stream()
                .filter(c -> c.getType() == type)
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByValueStart(String valueStart) {
        return getAllContact().stream()
                .filter(c -> c.getValue().startsWith(valueStart))
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByName(String namePart) {
        return getAllContact().stream()
                .filter(c -> c.getFullName().contains(namePart))
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Contact> findByValue(String value) {
        return getAllContact().stream()
                .filter(c -> c.getValue().equals(value))
                .findFirst();
    }

    @Override
    public void deleteByValue(String value) {
        List<Contact> temp = getAllContact().stream()
                .filter(c -> !c.getValue().equals(value))
                .collect(Collectors.toList());
        rewriteContactInFile(temp);
    }

    @Override
    public void save(Contact contact) {
        contact.setId(UUID.randomUUID().toString());
        writeContactInFile(contact);
    }

    private void writeContactInFile(Contact c) {
        try (PrintWriter writer = new PrintWriter(new FileWriter(path, true))) {
            writer.printf("%s:%s:%s:%s:%s\n", c.getId(), c.getName(), c.getSurname(), c.getType(), c.getValue());
            writer.flush();
        } catch (IOException exception) {
        }
    }

    private void rewriteContactInFile(List<Contact> contacts) {
        try (PrintWriter writer = new PrintWriter(new FileWriter(path))) {
            for (Contact c : contacts) {
                writer.printf("%s:%s:%s:%s:%s\n", c.getId(), c.getName(), c.getSurname(), c.getType(), c.getValue());
                writer.flush();
            }
        } catch (IOException exception) {
        }
    }

    private List<Contact> getAllContact() {
        List<Contact> contacts = new ArrayList<>();
        try (BufferedReader r = new BufferedReader(new FileReader(path))) {
            while (true) {
                String line = r.readLine();
                if (line == null) break;
                contacts.add(getContactFromText(line));
            }
        } catch (IOException | IndexOutOfBoundsException e) {
        }
        return contacts;
    }

    private Contact getContactFromText(String line) {
        String[] parts = line.split(":");
        return new Contact()
                .setId(parts[0])
                .setName(parts[1])
                .setSurname(parts[2])
                .setType(ContactType.valueOf(parts[3]))
                .setValue(parts[4]);
    }
}
