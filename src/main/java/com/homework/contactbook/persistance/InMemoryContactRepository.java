package com.homework.contactbook.persistance;

import com.homework.contactbook.model.Contact;
import com.homework.contactbook.model.ContactType;
import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@AllArgsConstructor
public class InMemoryContactRepository implements ContactRepository {

    List<Contact> contactsList = new ArrayList<>();

    @Override
    public List<Contact> findAll() {
        return contactsList.stream()
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByType(ContactType type) {
        return contactsList.stream()
                .filter(c -> c.getType() == type)
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByValueStart(String valueStart) {
        return contactsList.stream()
                .filter(c -> c.getValue().startsWith(valueStart))
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByName(String namePart) {
        return contactsList.stream()
                .filter(c -> c.getFullName().contains(namePart))
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Contact> findByValue(String value) {
        return contactsList.stream()
                .filter(c -> c.getValue().equals(value))
                .findFirst();
    }

    @Override
    public void deleteByValue(String value) {
        contactsList.removeIf(c -> c.getValue().equals(value));
    }

    @Override
    public void save(Contact contact) {
        contact.setId(UUID.randomUUID().toString());
        contactsList.add(contact);
    }
}
