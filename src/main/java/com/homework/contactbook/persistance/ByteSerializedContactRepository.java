package com.homework.contactbook.persistance;

import com.homework.contactbook.model.Contact;
import com.homework.contactbook.model.ContactType;
import lombok.AllArgsConstructor;

import java.io.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@AllArgsConstructor
public class ByteSerializedContactRepository implements ContactRepository {
    Path path;

    @Override
    public List<Contact> findAll() {
        return getAllContact().stream()
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByType(ContactType type) {
        return getAllContact().stream()
                .filter(c -> c.getType() == type)
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByValueStart(String valueStart) {
        return getAllContact().stream()
                .filter(c -> c.getValue().startsWith(valueStart))
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByName(String namePart) {
        return getAllContact().stream()
                .filter(c -> c.getFullName().contains(namePart))
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Contact> findByValue(String value) {
        return getAllContact().stream()
                .filter(c -> c.getValue().equals(value))
                .findFirst();
    }

    @Override
    public void deleteByValue(String value) {
        writeList(getAllExcept(value));
    }

    @Override
    public void save(Contact contact) {
        List<Contact> list = new ArrayList<>(getAllContact());
        contact.setId(UUID.randomUUID().toString());
        list.add(contact);
        writeList(list);
    }

    private void writeList(List<Contact> list) {
        try (ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(String.valueOf(path)))) {
            os.writeObject(list);
        } catch (IOException ex) {
        }
    }

    private List<Contact> getAllContact() {
        List<Contact> contacts = new ArrayList<>();
        try (ObjectInputStream os = new ObjectInputStream(new FileInputStream(String.valueOf(path)))) {
            contacts.addAll((List<Contact>) os.readObject());
        } catch (IOException | ClassNotFoundException e) {
        }
        return contacts;
    }

    private List<Contact> getAllExcept(String value) {
        return findAll().stream()
                .filter(c -> !c.getValue().equals(value))
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toList());
    }
}
