package com.homework.contactbook.persistance.database;

import com.homework.contactbook.model.Contact;
import com.homework.contactbook.model.ContactType;
import com.homework.contactbook.persistance.ContactRepository;
import com.homework.contactbook.service.authservice.DataBaseAuthService;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class DataBaseContactRepository implements ContactRepository {
    private final JdbcTemplate jdbcTemplate;
    private final DataBaseAuthService authService;

    @Override
    public List<Contact> findAll() {
        return jdbcTemplate.query(
                "SELECT id, name, surname, type, value FROM contacts WHERE user_id = ?",
                new Object[]{authService.getUserId()},
                new ContactPropertyRowMapper<>(Contact.class)
        );
    }

    @Override
    public List<Contact> findByType(ContactType type) {
        System.out.println(type);
        return jdbcTemplate.query(
                "SELECT id, name, surname, type, value FROM contacts WHERE user_id = ? AND type = ?",
                new Object[]{authService.getUserId(), type.toString()},
                new ContactPropertyRowMapper<>(Contact.class)
        );
    }

    @Override
    public List<Contact> findByValueStart(String valueStart) {
        return jdbcTemplate.query(
                "SELECT id, name, surname, type, value FROM contacts WHERE value LIKE ?",
                new Object[]{valueStart + "%"},
                new ContactPropertyRowMapper<>(Contact.class)
        );
    }

    @Override
    public List<Contact> findByName(String namePart) {
        return jdbcTemplate.query(
                "SELECT id, name, surname, type, value FROM contacts WHERE user_id = ? AND name LIKE ?",
                new Object[]{authService.getUserId(), namePart + "%"},
                new ContactPropertyRowMapper<>(Contact.class)
        );
    }

    @Override
    public Optional<Contact> findByValue(String value) {
        return jdbcTemplate.query(
                "SELECT id, name, surname, type, value FROM contacts WHERE user_id = ? AND value = ?",
                new Object[]{authService.getUserId(), value},
                new ContactPropertyRowMapper<>(Contact.class)
        ).stream().findFirst();
    }

    @Override
    public void deleteByValue(String value) {
        jdbcTemplate.update(
                "DELETE FROM contacts WHERE user_id = ? AND value = ?",
                new Object[]{authService.getUserId(), value}
        );
    }

    @Override
    public void save(Contact contact) {
        jdbcTemplate.update(
                "INSERT INTO contacts (name, surname, type, value, user_id) VALUES (?, ?, ?, ?, ?)",
                new Object[]{
                        contact.getName(),
                        contact.getSurname(),
                        contact.getType().toString(),
                        contact.getValue(),
                        authService.getUserId()
                }
        );
    }
}
