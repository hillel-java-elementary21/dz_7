package com.homework.contactbook.persistance.database.userrepository;

import com.homework.contactbook.model.User;

import java.util.List;

public interface UserRepository {

    void save(User user);

    List<User> findByLogin(String login);

    void delete(User user);

}
