package com.homework.contactbook.persistance.database;

import com.homework.contactbook.model.User;
import lombok.RequiredArgsConstructor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RequiredArgsConstructor
public class UserPropertyRowMapper<T> implements Function<ResultSet, T> {
    private final Class<T> clazz;

    @Override
    public T apply(ResultSet resultSet) {
        User user = new User();
        try {
            user.setId(resultSet.getInt("id"));
            user.setLogin(resultSet.getString("login"));
            user.setPassword(resultSet.getString("password"));
            user.setDateBorn(resultSet.getString("date_born"));
            return (T) user;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String toSnakeCase(String string) {
        Pattern p = Pattern.compile("\\B([A-Z])");
        Matcher m = p.matcher(string);
        StringBuilder stringBuilder = new StringBuilder();
        while (m.find()) {
            m.appendReplacement(stringBuilder, "_$0");
        }
        m.appendTail(stringBuilder);
        string = stringBuilder.toString().toLowerCase();
        return string;
    }
}
