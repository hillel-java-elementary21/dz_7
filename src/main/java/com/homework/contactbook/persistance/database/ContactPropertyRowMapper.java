package com.homework.contactbook.persistance.database;

import com.homework.contactbook.model.Contact;
import com.homework.contactbook.model.ContactType;
import lombok.RequiredArgsConstructor;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Function;

@RequiredArgsConstructor
public class ContactPropertyRowMapper<T> implements Function<ResultSet, T> {
    private final Class<T> clazz;

    @Override
    public T apply(ResultSet resultSet) {
        Contact contact = new Contact();
        try {
            contact.setId(resultSet.getString("id"));
            contact.setName(resultSet.getString("name"));
            contact.setSurname(resultSet.getString("surname"));
            contact.setType(ContactType.valueOf(resultSet.getString("type").toUpperCase()));
            contact.setValue(resultSet.getString("value"));
            return (T) contact;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public T apply2(ResultSet resultSet) {
        Field[] fields = clazz.getDeclaredFields();

        try {
            T instance = clazz.getConstructor().newInstance();
            for (Field field : fields) {
                String name = field.getName(); //TODO: to snake case
                Object value = resultSet.getObject(name, field.getType());
                field.setAccessible(true);
                field.set(instance, value);
            }
            return instance;
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
