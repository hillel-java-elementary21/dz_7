package com.homework.contactbook.persistance.database.userrepository;

import com.homework.contactbook.model.User;
import com.homework.contactbook.persistance.database.JdbcTemplate;
import com.homework.contactbook.persistance.database.UserPropertyRowMapper;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class DataBaseUserRepository implements UserRepository {
    private final JdbcTemplate jdbcTemplate;

    public List<User> getAll() {
        return jdbcTemplate.query(
                "SELECT id, login, password, register_date FROM users",
                new UserPropertyRowMapper<>(User.class));
    }

    @Override
    public void save(User user) {
        jdbcTemplate.update(
                "INSERT INTO users (login, password, date_born) VALUES (?, ?, ?)",
                new Object[]{user.getLogin(), user.getPassword(), user.getDateBorn()}
        );
    }

    @Override
    public List<User> findByLogin(String login) {
        return jdbcTemplate.query(
                "SELECT id, login, password, date_born FROM users WHERE login = ?",
                new Object[]{login},
                new UserPropertyRowMapper<>(User.class)
        );
    }

    @Override
    public void delete(User user) {
        jdbcTemplate.update(
                "DELETE FROM users WHERE id = ?",
                new Object[]{user.getId()}
        );
    }
}
