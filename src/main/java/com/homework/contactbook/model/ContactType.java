package com.homework.contactbook.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ContactType {
    @JsonProperty("email")
    EMAIL("E-mail"),
    @JsonProperty("phone")
    PHONE("Phone");

    @Getter
    private final String displayName;
}
