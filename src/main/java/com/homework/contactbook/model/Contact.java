package com.homework.contactbook.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Objects;

@Data
@Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Contact implements Serializable {
    private String id;
    private String name;
    @JsonIgnore
    private String surname;
    private ContactType type;
    private String value;

    public static Comparator<Contact> bySurnameComparator() {
        return (Contact a, Contact b) -> {
            if (a == null && b == null) return 0;
            if (a == null) return -1;
            if (b == null) return 1;

            int cmp = Objects.compare(a.surname, b.surname, String::compareTo);
            if (cmp == 0) {
                cmp = Objects.compare(a.name, b.name, String::compareTo);
            }
            return cmp;
        };
    }

    @JsonIgnore
    public String getFullName() {
        return surname + " " + name;
    }
}
