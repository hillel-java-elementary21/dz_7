package com.homework.contactbook.ui.menu.item.networkitem;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.homework.contactbook.exception.ContactNotFoundException;
import com.homework.contactbook.service.authservice.AuthService;
import com.homework.contactbook.service.contactservice.NetworkContactService;
import com.homework.contactbook.ui.menu.MenuItem;
import com.homework.contactbook.ui.menu.view.ContactView;
import com.homework.contactbook.ui.menu.view.ErrorView;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FindContactByValueNetworkMenuItem implements MenuItem {
    private final AuthService authService;
    private final NetworkContactService contactService;
    private final ContactView contactView;
    private final ErrorView errorView;

    @Override
    public String getName() {
        return "Поиск по значению";
    }

    @Override
    public void execute() {
        try {
            contactView.showContacts(contactService.findByValue(contactView.readRequest()));
            contactView.pause();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (ContactNotFoundException exception) {
            errorView.showError("Контакты с таким значением не найдены!");
        }
    }

    @Override
    public boolean condition() {
        return authService.isAuth();
    }
}
