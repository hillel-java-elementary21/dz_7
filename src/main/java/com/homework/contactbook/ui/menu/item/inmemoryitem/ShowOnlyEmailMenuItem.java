package com.homework.contactbook.ui.menu.item.inmemoryitem;

import com.homework.contactbook.model.ContactType;
import com.homework.contactbook.service.authservice.AuthService;
import com.homework.contactbook.service.contactservice.ContactService;
import com.homework.contactbook.ui.menu.MenuItem;
import com.homework.contactbook.ui.menu.view.ContactView;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ShowOnlyEmailMenuItem implements MenuItem {
    private final ContactService contactService;
    private final ContactView contactView;
    private final AuthService authService;


    @Override
    public String getName() {
        return "Посмотреть адреса электронной почты";
    }

    @Override
    public void execute() {
        contactView.showEmails(contactService.findEmails(ContactType.EMAIL));
        contactView.pause();
    }

    @Override
    public boolean condition() {
        return authService.isAuth();
    }
}
