package com.homework.contactbook.ui.menu;

public interface MenuItem {

    String getName();

    void execute();

    default boolean ifFinal() {
        return false;
    }

    default boolean condition() {
        return false;
    }
}
