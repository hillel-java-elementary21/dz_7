package com.homework.contactbook.ui.menu.item.networkitem;

import com.homework.contactbook.service.authservice.AuthService;
import com.homework.contactbook.service.contactservice.NetworkContactService;
import com.homework.contactbook.ui.menu.MenuItem;
import com.homework.contactbook.ui.menu.view.ContactView;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ShowAllContactNetworkMenuItem implements MenuItem {
    private final AuthService authService;
    private final NetworkContactService contactService;
    private final ContactView contactView;

    @Override
    public String getName() {
        return "Посмотреть все контакты";
    }

    @Override
    public void execute() {
        contactView.showContacts(contactService.findAll());
        contactView.pause();
    }

    @Override
    public boolean condition() {
        return authService.isAuth();
    }
}
