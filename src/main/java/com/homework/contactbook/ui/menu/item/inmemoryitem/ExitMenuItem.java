package com.homework.contactbook.ui.menu.item.inmemoryitem;

import com.homework.contactbook.ui.menu.MenuItem;

public class ExitMenuItem implements MenuItem {
    @Override
    public String getName() {
        return "Выход";
    }

    @Override
    public void execute() {
        System.out.println("Программа завершила свою работу. До скорых встреч...");
    }

    @Override
    public boolean ifFinal() {
        return true;
    }

    @Override
    public boolean condition() {
        return true;
    }
}
