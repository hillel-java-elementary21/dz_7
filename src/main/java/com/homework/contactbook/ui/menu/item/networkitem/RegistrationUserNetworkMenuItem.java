package com.homework.contactbook.ui.menu.item.networkitem;

import com.homework.contactbook.exception.InvalidInputException;
import com.homework.contactbook.exception.RegistrationException;
import com.homework.contactbook.model.User;
import com.homework.contactbook.service.authservice.AuthService;
import com.homework.contactbook.ui.menu.MenuItem;
import com.homework.contactbook.ui.menu.view.ErrorView;
import com.homework.contactbook.ui.menu.view.UserView;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class RegistrationUserNetworkMenuItem implements MenuItem {
    private final AuthService authService;
    private final UserView userView;
    private final ErrorView errorView;

    @Override
    public String getName() {
        return "Регистрация нового пользователя";
    }

    @Override
    public void execute() {
        try {
            User user = userView.registrationUser();
            authService.register(user);

        } catch (InvalidInputException exception) {
            errorView.showError("Ошибка данных");
        } catch (RegistrationException exception) {
            errorView.showError(exception.getError());
        }
    }

    @Override
    public boolean condition() {
        return !authService.isAuth();
    }
}
