package com.homework.contactbook.ui.menu.view;

public class ErrorView {
    public void showError(String message) {
        System.out.println("x".repeat(50));
        System.out.printf("X %-46s X\n", message);
        System.out.println("x".repeat(50));
    }
}

