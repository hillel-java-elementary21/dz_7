package com.homework.contactbook.ui.menu.item.inmemoryitem;

import com.homework.contactbook.service.authservice.AuthService;
import com.homework.contactbook.ui.menu.MenuItem;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class LogoutUserMenuItem implements MenuItem {
    private final AuthService authService;

    @Override
    public String getName() {
        return "Выход из учетной записи";
    }

    @Override
    public void execute() {
        authService.logout();
    }

    @Override
    public boolean condition() {
        return authService.isAuth();
    }
}
