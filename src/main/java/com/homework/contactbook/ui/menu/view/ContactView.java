package com.homework.contactbook.ui.menu.view;

import com.homework.contactbook.model.Contact;

import java.util.List;

public interface ContactView {
    Contact readContact();

    String readRequest();

    void showContacts(List<Contact> contactList);

    void showPhones(List<Contact> contactList);

    void showEmails(List<Contact> contactList);

    void pause();
}
