package com.homework.contactbook.ui.menu.item.networkitem;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.homework.contactbook.exception.DuplicateContactException;
import com.homework.contactbook.exception.InvalidInputException;
import com.homework.contactbook.exception.ValidatorException;
import com.homework.contactbook.model.Contact;
import com.homework.contactbook.service.authservice.AuthService;
import com.homework.contactbook.service.contactservice.NetworkContactService;
import com.homework.contactbook.ui.menu.MenuItem;
import com.homework.contactbook.ui.menu.view.ContactView;
import com.homework.contactbook.ui.menu.view.ErrorView;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AddContactNetworkMenuItem implements MenuItem {
    private final AuthService authService;
    private final NetworkContactService contactService;
    private final ContactView contactView;
    private final ErrorView errorView;

    @Override
    public String getName() {
        return "Добавить контакт";
    }

    @Override
    public void execute() {
        try {
            Contact contact = contactView.readContact();
            contactService.save(contact);
        } catch (InvalidInputException exception) {
            errorView.showError("Ошибка данных");
        } catch (DuplicateContactException exception) {
            errorView.showError("Ошибка! Контак уже существует!");
        } catch (ValidatorException exception) {
            errorView.showError("Не верная форма контакта. Контакт не создан");
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean condition() {
        return authService.isAuth();
    }
}
