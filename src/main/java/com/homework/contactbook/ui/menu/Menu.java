package com.homework.contactbook.ui.menu;

import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@RequiredArgsConstructor
public class Menu {
    private final List<MenuItem> items;
    private final Scanner scanner;
    private List<MenuItem> itemsCondition = new ArrayList<>();


    public void run() {
        while (true) {
            showMenu();
            int choice = getUserChoice();
            if (choice >= 0 && choice < itemsCondition.size()) {
                MenuItem item = itemsCondition.get(choice);
                item.execute();
                if (item.ifFinal()) return;
            } else {
                System.out.println("Ошибочный выбор. Повторите ввод...");
                scanner.nextLine();
            }
        }
    }

    private int getUserChoice() {
        System.out.print("Введите свой выбор: ");
        if (scanner.hasNextInt()) {
            int ch = scanner.nextInt();
            scanner.nextLine();
            return ch - 1;
        } else {
            scanner.nextLine();
            return itemsCondition.size() + 1;
        }
    }

    private void showMenu() {
        System.out.println("------------------Меню-------------------");
        itemsCondition.clear();
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).condition()) {
                itemsCondition.add(items.get(i));
            }
        }
        for (int i = 0; i < itemsCondition.size(); i++) {
            System.out.printf("%2d - %s\n", i + 1, itemsCondition.get(i).getName());
        }

        System.out.println("-".repeat(42));
    }
}
