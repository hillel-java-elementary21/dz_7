package com.homework.contactbook.ui.menu.item.inmemoryitem;

import com.homework.contactbook.exception.AuthorisationException;
import com.homework.contactbook.exception.DuplicateContactException;
import com.homework.contactbook.exception.InvalidInputException;
import com.homework.contactbook.model.User;
import com.homework.contactbook.service.authservice.AuthService;
import com.homework.contactbook.ui.menu.MenuItem;
import com.homework.contactbook.ui.menu.view.ErrorView;
import com.homework.contactbook.ui.menu.view.UserView;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class RegistrationUserMenuItem implements MenuItem {
    private final UserView userView;
    private final ErrorView errorView;
    private final AuthService authService;

    @Override
    public String getName() {
        return "Регистрация";
    }

    @Override
    public void execute() {
        try {
            User user = userView.registrationUser();
            authService.register(user);
        } catch (InvalidInputException exception) {
            errorView.showError("Ошибка данных");
        } catch (AuthorisationException exception) {
            errorView.showError(exception.getError());
        } catch (DuplicateContactException exception) {
            errorView.showError("Пользователь с таким логином уже существует!");
        }
    }

    @Override
    public boolean condition() {
        return !authService.isAuth();
    }
}
