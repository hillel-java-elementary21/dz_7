package com.homework.contactbook.ui.menu.view;

import com.homework.contactbook.model.User;

public interface UserView {
    User authorisationUser();

    User registrationUser();

    void pause();
}
