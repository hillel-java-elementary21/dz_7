package com.homework.contactbook.ui.menu.item.inmemoryitem;

import com.homework.contactbook.model.ContactType;
import com.homework.contactbook.service.authservice.AuthService;
import com.homework.contactbook.service.contactservice.ContactService;
import com.homework.contactbook.ui.menu.MenuItem;
import com.homework.contactbook.ui.menu.view.ContactView;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ShowOnlyNumberMenuItem implements MenuItem {
    private final ContactService contactService;
    private final ContactView contactView;
    private final AuthService authService;


    @Override
    public String getName() {
        return "Посмотреть телефонные номера";
    }

    @Override
    public void execute() {
        contactView.showPhones(contactService.findPhone(ContactType.PHONE));
        contactView.pause();
    }

    @Override
    public boolean condition() {
        return authService.isAuth();
    }
}
