package com.homework.contactbook.ui.menu.item.networkitem;

import com.homework.contactbook.exception.AuthorisationException;
import com.homework.contactbook.exception.InvalidInputException;
import com.homework.contactbook.model.User;
import com.homework.contactbook.service.authservice.AuthService;
import com.homework.contactbook.ui.menu.MenuItem;
import com.homework.contactbook.ui.menu.view.ErrorView;
import com.homework.contactbook.ui.menu.view.UserView;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AuthorizationUserNetworkMenuItem implements MenuItem {
    private final AuthService authService;
    private final UserView userView;
    private final ErrorView errorView;

    @Override
    public String getName() {
        return "Авторизация/вход";
    }

    @Override
    public void execute() {
        try {
            User user = userView.authorisationUser();
            authService.login(user);
        } catch (InvalidInputException exception) {
            errorView.showError("Ошибка данных");
        } catch (AuthorisationException exception) {
            errorView.showError(exception.getError());
        }
    }

    @Override
    public boolean condition() {
        return !authService.isAuth();
    }
}
