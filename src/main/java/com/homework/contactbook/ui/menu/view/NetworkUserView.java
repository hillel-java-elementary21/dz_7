package com.homework.contactbook.ui.menu.view;

import com.homework.contactbook.model.User;
import lombok.RequiredArgsConstructor;

import java.util.Scanner;

@RequiredArgsConstructor
public class NetworkUserView implements UserView {
    private final Scanner scanner;

    @Override
    public User authorisationUser() {
        System.out.print("Введите имя пользователя/логин: ");
        String login = scanner.nextLine();
        System.out.print("Введите пароль: ");
        String password = scanner.nextLine();
        return new User()
                .setLogin(login)
                .setPassword(password);
    }

    @Override
    public User registrationUser() {
        System.out.print("Введите имя пользователя/логин: ");
        String login = scanner.nextLine();
        System.out.print("Введите дату рождения (ГГГГ-ММ-ДД): ");
        String dateBorn = scanner.nextLine();
        System.out.print("Введите пароль: ");
        String password = scanner.nextLine();
        return new User()
                .setLogin(login)
                .setPassword(password)
                .setDateBorn(dateBorn);
    }


    @Override
    public void pause() {
        System.out.print("Нажмите ENTER чтобы продолжить...");
        scanner.nextLine();
    }
}
