package com.homework.contactbook.ui.menu.item.inmemoryitem;

import com.homework.contactbook.exception.ContactNotFoundException;
import com.homework.contactbook.service.authservice.AuthService;
import com.homework.contactbook.service.contactservice.ContactService;
import com.homework.contactbook.ui.menu.MenuItem;
import com.homework.contactbook.ui.menu.view.ContactView;
import com.homework.contactbook.ui.menu.view.ErrorView;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FindByValueStartMenuItem implements MenuItem {
    private final ContactService contactService;
    private final ContactView contactView;
    private final ErrorView errorView;
    private final AuthService authService;

    @Override
    public String getName() {
        return "Поиск по началу контакта";
    }

    @Override
    public void execute() {
        try {
            contactView.showContacts(contactService.findByValueStart(contactView.readRequest()));
            contactView.pause();
        } catch (ContactNotFoundException e) {
            errorView.showError("По вашему запросу контакты не найдены!");
        }
    }

    @Override
    public boolean condition() {
        return authService.isAuth();
    }
}
