package com.homework.contactbook.ui.menu.item.inmemoryitem;

import com.homework.contactbook.exception.ContactNotFoundException;
import com.homework.contactbook.exception.InvalidInputException;
import com.homework.contactbook.service.authservice.AuthService;
import com.homework.contactbook.service.contactservice.ContactService;
import com.homework.contactbook.ui.menu.MenuItem;
import com.homework.contactbook.ui.menu.view.ContactView;
import com.homework.contactbook.ui.menu.view.ErrorView;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class RemoveContactMenuItem implements MenuItem {
    private final ContactService contactService;
    private final ContactView contactView;
    private final ErrorView errorView;
    private final AuthService authService;

    @Override
    public String getName() {
        return "Удалить контакт";
    }

    @Override
    public void execute() {
        try {
            contactService.deleteByValue(contactView.readRequest());
            System.out.println("Контак успешно удален!");
            contactView.pause();
        } catch (InvalidInputException exception) {
            errorView.showError("Ошибка данных");
        } catch (ContactNotFoundException exception) {
            errorView.showError("Контак не существует! Удаление невозможно.");
        }
    }

    @Override
    public boolean condition() {
        return authService.isAuth();
    }
}
