package com.homework.contactbook.ui.menu.view;

import com.homework.contactbook.exception.InvalidInputException;
import com.homework.contactbook.model.Contact;
import com.homework.contactbook.model.ContactType;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Scanner;

@RequiredArgsConstructor
public class DefaultContactView implements ContactView {
    private final Scanner scanner;

    @Override
    public Contact readContact() {
        System.out.print("Введите имя контакта: ");
        String name = scanner.nextLine();
        System.out.print("Введите фамилию контакта: ");
        String surname = scanner.nextLine();
        ContactType type = readContactType();
        checkUserChoice(type);
        String value = scanner.nextLine();
        return new Contact()
                .setName(name)
                .setSurname(surname)
                .setType(type)
                .setValue(value);
    }

    @Override
    public String readRequest() {
        System.out.print("Введите значение: ");
        return scanner.nextLine();
    }

    @Override
    public void showContacts(List<Contact> contactList) {
        System.out.println("-".repeat(115));
        System.out.printf("|%-30s|%-13s|%-27s|%-40s|\n", "Имя", "Тип", "Значение", "Id");
        System.out.println("-".repeat(115));
        for (Contact contact : contactList) {
            System.out.printf("|%-30s|%-13s|%-27s|%-40s|\n",
                    contact.getFullName(),
                    contact.getType().getDisplayName(),
                    contact.getValue(),
                    contact.getId()
            );
        }
        System.out.println("-".repeat(115));
    }

    @Override
    public void showPhones(List<Contact> contactList) {
        System.out.println("-".repeat(29));
        System.out.printf("|%-27s|\n", "Телефонные номера");
        System.out.println("-".repeat(29));
        for (Contact contact : contactList) {
            System.out.printf("|%-27s|\n",
                    contact.getValue()
            );
        }
        System.out.println("-".repeat(29));
    }

    @Override
    public void showEmails(List<Contact> contactList) {
        System.out.println("-".repeat(29));
        System.out.printf("|%-27s|\n", "Адреса электронной почты ");
        System.out.println("-".repeat(29));
        for (Contact contact : contactList) {
            System.out.printf("|%-27s|\n",
                    contact.getValue()
            );
        }
        System.out.println("-".repeat(29));
    }

    @Override
    public void pause() {
        System.out.print("Нажмите ENTER чтобы продолжить...");
        scanner.nextLine();
    }

    private ContactType readContactType() {
        ContactType[] types = ContactType.values();
        for (int i = 0; i < types.length; i++) {
            System.out.printf("%d - %s\n", i + 1, types[i].getDisplayName());
        }
        int choice = getChoice();
        if (choice < 0 || choice >= types.length) {
            throw new InvalidInputException();
        }
        return types[choice];
    }

    private int getChoice() {
        System.out.print("Введите свой выбор: ");
        if (scanner.hasNextInt()) {
            int choice = scanner.nextInt() - 1;
            scanner.nextLine();
            return choice;
        } else {
            scanner.nextLine();
            throw new InvalidInputException();
        }
    }

    private void checkUserChoice(ContactType type) {
        if (type == ContactType.EMAIL) System.out.print("Введите адресс электронной почты: ");
        if (type == ContactType.PHONE) System.out.print("Введите номер телефона: ");
    }
}
