package com.homework.contactbook.service;

import lombok.SneakyThrows;

import java.math.BigInteger;
import java.security.MessageDigest;

public class Md5PasswordEncoder implements PasswordEncoder {
    @SneakyThrows
    @Override
    public String encode(String password) {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(password.getBytes());
        byte[] digest = md.digest();
        return new BigInteger(1, digest).toString(16).toUpperCase();
    }

    @Override
    public boolean verify(String password, String hash) {
        return hash.equals(encode(password));
    }
}