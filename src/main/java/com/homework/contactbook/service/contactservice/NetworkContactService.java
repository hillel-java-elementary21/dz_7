package com.homework.contactbook.service.contactservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.homework.contactbook.exception.DuplicateContactException;
import com.homework.contactbook.exception.ValidatorException;
import com.homework.contactbook.model.ApiResponse;
import com.homework.contactbook.model.Contact;
import com.homework.contactbook.service.authservice.NetworkAuthService;
import com.homework.contactbook.utills.validator.Validator;
import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class NetworkContactService {
    private final ObjectMapper json;
    private final HttpClient httpClient;
    private final NetworkAuthService authService;
    private final Validator<Contact> contactValidator;

    public List<Contact> findAll() {
        List<Contact> contacts = new ArrayList<>();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://mag-contacts-api.herokuapp.com/contacts"))
                .header("Authorization", "Bearer " + authService.getToken())
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .GET()
                .build();
        try {
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            ApiResponse apiResponse = json.readValue(response.body(), ApiResponse.class);
            contacts.addAll(apiResponse.getContacts());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return contacts;
    }

    public List<Contact> findByValue(String value) throws JsonProcessingException {
        List<Contact> contacts = new ArrayList<>();
        String jsonString = ("{\"value\" : \"" + value + "\"}");
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://mag-contacts-api.herokuapp.com/contacts/find"))
                .header("Authorization", "Bearer " + authService.getToken())
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(jsonString))
                .build();
        try {
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            ApiResponse apiResponse = json.readValue(response.body(), ApiResponse.class);
            contacts.addAll(apiResponse.getContacts());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return contacts;
    }

    public List<Contact> findByName(String name) throws JsonProcessingException {
        List<Contact> contacts = new ArrayList<>();
        String jsonString = ("{\"name\" : \"" + name + "\"}");
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://mag-contacts-api.herokuapp.com/contacts/find"))
                .header("Authorization", "Bearer " + authService.getToken())
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(jsonString))
                .build();
        try {
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            ApiResponse apiResponse = json.readValue(response.body(), ApiResponse.class);
            contacts.addAll(apiResponse.getContacts());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return contacts;
    }

    public void save(Contact contact) throws JsonProcessingException {
        checkValidContact(contact);
        checkDuplicateValueContact(contact);
        String jsonString = json.writeValueAsString(contact);
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://mag-contacts-api.herokuapp.com/contacts/add"))
                .header("Authorization", "Bearer " + authService.getToken())
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(jsonString))
                .build();
        try {
            httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void checkValidContact(Contact contact) {
        if (!contactValidator.isValid(contact)) throw new ValidatorException();
    }

    private void checkDuplicateValueContact(Contact contact) throws JsonProcessingException {
        if (!findByValue(contact.getValue()).isEmpty()) throw new DuplicateContactException();
    }
}
