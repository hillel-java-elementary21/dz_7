package com.homework.contactbook.service.contactservice;

import com.homework.contactbook.exception.ContactNotFoundException;
import com.homework.contactbook.exception.DuplicateContactException;
import com.homework.contactbook.exception.ValidatorException;
import com.homework.contactbook.model.Contact;
import com.homework.contactbook.model.ContactType;
import com.homework.contactbook.persistance.ContactRepository;
import com.homework.contactbook.utills.validator.Validator;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class ContactService {
    private final ContactRepository contactRepository;
    private final Validator<Contact> contactValidator;

    public List<Contact> findAll() {
        return contactRepository.findAll();
    }

    public List<Contact> findEmails(ContactType type) {
        return contactRepository.findByType(ContactType.EMAIL);
    }

    public List<Contact> findPhone(ContactType type) {
        return contactRepository.findByType(ContactType.PHONE);
    }

    public List<Contact> findByValueStart(String valueStart) {
        if (contactRepository.findByValueStart(valueStart).isEmpty()) {
            throw new ContactNotFoundException();
        }
        return contactRepository.findByValueStart(valueStart);
    }

    public List<Contact> findByName(String namePart) {
        if (contactRepository.findByName(namePart).isEmpty()) {
            throw new ContactNotFoundException();
        }
        return contactRepository.findByName(namePart);
    }

    public void deleteByValue(String value) {
        contactRepository.findByValue(value)
                .orElseThrow(ContactNotFoundException::new);
        contactRepository.deleteByValue(value);
    }

    public void save(Contact contact) {
        if (!contactValidator.isValid(contact)) {
            throw new ValidatorException();
        }
        if (contactRepository.findByValue(contact.getValue()).isPresent()) {
            throw new DuplicateContactException();
        }
        contactRepository.save(contact);
    }
}
