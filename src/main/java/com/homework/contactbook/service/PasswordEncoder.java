package com.homework.contactbook.service;

public interface PasswordEncoder {

    String encode(String password);

    boolean verify(String password, String hash);
}
