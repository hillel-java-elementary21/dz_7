package com.homework.contactbook.service.authservice;

import com.homework.contactbook.exception.AuthorisationException;
import com.homework.contactbook.model.User;

public class InMemoryAuthService implements AuthService {
    private final static String LOGIN = "admin";
    private final static String PASSWORD = "admin";
    private boolean auth = false;

    @Override
    public void login(User user) {
        if (!user.getLogin().equals(LOGIN) || !user.getPassword().equals(PASSWORD)) {
            throw new AuthorisationException("Неверный логин или пароль!");
        }
        auth = true;
    }

    @Override
    public void register(User user) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void logout() {
        auth = false;
    }

    @Override
    public boolean isAuth() {
        return auth;
    }
}
