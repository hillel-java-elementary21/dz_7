package com.homework.contactbook.service.authservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.homework.contactbook.exception.AuthorisationException;
import com.homework.contactbook.exception.RegistrationException;
import com.homework.contactbook.model.ApiResponse;
import com.homework.contactbook.model.User;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@RequiredArgsConstructor
public class NetworkAuthService implements AuthService {
    private final ObjectMapper json;
    private final HttpClient httpClient;

    @Getter
    private String token;

    @Override
    public void login(User user) {
        try {
            String jsonString = json.writeValueAsString(user);
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create("https://mag-contacts-api.herokuapp.com/login"))
                    .header("Accept", "application/json")
                    .header("Content-Type", "application/json")
                    .POST(HttpRequest.BodyPublishers.ofString(jsonString))
                    .build();
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            ApiResponse apiResponse = json.readValue(response.body(), ApiResponse.class);
            setToken(apiResponse);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void register(User user) {
        try {
            String jsonString = json.writeValueAsString(user);
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create("https://mag-contacts-api.herokuapp.com/register"))
                    .header("Accept", "application/json")
                    .header("Content-Type", "application/json")
                    .POST(HttpRequest.BodyPublishers.ofString(jsonString))
                    .build();
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            ApiResponse apiResponse = json.readValue(response.body(), ApiResponse.class);
            if (apiResponse.getStatus().equals("error")) {
                throw new RegistrationException(apiResponse.getError());
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void logout() {
        token = null;
    }

    @Override
    public boolean isAuth() {
        return token != null;
    }

    private void setToken(ApiResponse apiResponse) {
        if (apiResponse.getStatus().equals("ok")) {
            token = apiResponse.getToken();
        } else if (apiResponse.getStatus().equals("error")) {
            throw new AuthorisationException(apiResponse.getError());
        }
    }
}
