package com.homework.contactbook.service.authservice;

import com.homework.contactbook.exception.AuthorisationException;
import com.homework.contactbook.exception.DuplicateContactException;
import com.homework.contactbook.model.User;
import com.homework.contactbook.persistance.database.userrepository.UserRepository;
import com.homework.contactbook.service.PasswordEncoder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DataBaseAuthService implements AuthService {
    private final UserRepository userRepository;
    private final PasswordEncoder encoder;

    @Getter
    protected Integer userId;

    @Override
    public void login(User user) {

        if (userRepository.findByLogin(user.getLogin()).isEmpty()) {
            throw new AuthorisationException("Пользователь не существует!");
        }
        User userFromBase = getUserByLogin(user.getLogin());
        if (!encoder.verify(user.getPassword(), userFromBase.getPassword())) {
            throw new AuthorisationException("Неверный логин или пароль!");
        }
        setUserId(user.getLogin());
    }

    @Override
    public void register(User user) {
        if ((userRepository.findByLogin(user.getLogin())).isEmpty()) {
            user.setPassword(encoder.encode(user.getPassword()));
            userRepository.save(user);
        } else {
            throw new DuplicateContactException();
        }
    }


    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    private void setUserId(String login) {
        for (User user : userRepository.findByLogin(login)) {
            userId = user.getId();
        }
    }

    private User getUserByLogin(String login) {
        for (User user : userRepository.findByLogin(login)) {
            return user;
        }
        return null;
    }
}
