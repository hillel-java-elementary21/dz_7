package com.homework.contactbook.service.authservice;

import com.homework.contactbook.model.User;

public interface AuthService {

    void login(User user);

    void register(User user);

    void logout();

    boolean isAuth();
}
