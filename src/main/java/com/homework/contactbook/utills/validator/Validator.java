package com.homework.contactbook.utills.validator;

public interface Validator<T> {
    boolean isValid(T object);

}
