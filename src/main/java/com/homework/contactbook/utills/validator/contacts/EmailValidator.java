package com.homework.contactbook.utills.validator.contacts;

import com.homework.contactbook.model.Contact;
import com.homework.contactbook.model.ContactType;
import com.homework.contactbook.utills.validator.Validator;

import java.util.regex.Pattern;

public class EmailValidator implements Validator<Contact> {
    private static final Pattern EMAIL_PATTERN = Pattern.compile("(([a-z0-9_-]+\\.)*[a-z0-9_-]+)@" +
            "([a-z0-9_-]+)" +
            "((?:\\.[a-z]{2,6}){1,2})");

    @Override
    public boolean isValid(Contact contact) {
        if (contact.getType() != ContactType.EMAIL) return true;
        return EMAIL_PATTERN.matcher(contact.getValue()).matches();
    }
}
