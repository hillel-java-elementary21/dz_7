package com.homework.contactbook.utills.validator.contacts;

import com.homework.contactbook.model.Contact;
import com.homework.contactbook.model.ContactType;
import com.homework.contactbook.utills.validator.Validator;

import java.util.regex.Pattern;

public class PhoneValidator implements Validator<Contact> {
    private static final Pattern PHONE_PATTERN = Pattern.compile("(\\+380|80|0)\\d{9}");

    @Override
    public boolean isValid(Contact contact) {
        if (contact.getType() != ContactType.PHONE) return true;
        return PHONE_PATTERN.matcher(contact.getValue()).matches();
    }
}
