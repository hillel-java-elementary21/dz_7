package com.homework.contactbook.utills.validator.contacts;

import com.homework.contactbook.model.Contact;
import com.homework.contactbook.utills.validator.Validator;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class ContactValidator implements Validator<Contact> {

    private final List<Validator<Contact>> validators;

    @Override
    public boolean isValid(Contact contact) {
        return validators.stream().allMatch(v -> v.isValid(contact));
    }
}
