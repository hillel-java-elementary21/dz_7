package com.homework.contactbook.factory;

import com.homework.contactbook.model.Contact;
import com.homework.contactbook.persistance.ContactRepository;
import com.homework.contactbook.persistance.database.DataBaseContactRepository;
import com.homework.contactbook.persistance.database.JdbcTemplate;
import com.homework.contactbook.persistance.database.userrepository.DataBaseUserRepository;
import com.homework.contactbook.persistance.database.userrepository.UserRepository;
import com.homework.contactbook.service.Md5PasswordEncoder;
import com.homework.contactbook.service.PasswordEncoder;
import com.homework.contactbook.service.authservice.AuthService;
import com.homework.contactbook.service.authservice.DataBaseAuthService;
import com.homework.contactbook.service.contactservice.ContactService;
import com.homework.contactbook.utills.validator.Validator;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.RequiredArgsConstructor;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@RequiredArgsConstructor
public class DataBaseServiceFactory implements ContactServiceFactory {
    private final Properties properties;
    private final Validator<Contact> contactValidator;
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;
    private UserRepository userRepository;
    private AuthService dataBaseAuthService;
    private ContactRepository contactRepository;

    @Override
    public ContactService createContactService() {
        return new ContactService(createContactRepository(), contactValidator);
    }

    @Override
    public AuthService createAuthService() {
        if (dataBaseAuthService == null) {
            dataBaseAuthService = new DataBaseAuthService(
                    getUserRepository(),
                    getPasswordEncoder()
            );
        }
        return dataBaseAuthService;
    }

    public ContactRepository createContactRepository() {
        if (contactRepository == null) {
            contactRepository = new DataBaseContactRepository(
                    getJdbcTemplate(),
                    (DataBaseAuthService) createAuthService()
            );
        }
        return contactRepository;
    }

    private HikariConfig getHikariConfig() {
        List<String> conf = new ArrayList<>(getSettings());
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(conf.get(0));
        hikariConfig.setUsername(conf.get(1));
        hikariConfig.setPassword(conf.get(2));
        return hikariConfig;
    }

    private DataSource getDataSource() {
        if (dataSource == null) {
            dataSource = new HikariDataSource(getHikariConfig());
        }
        return dataSource;
    }

    private UserRepository getUserRepository() {
        if (userRepository == null) {
            userRepository = new DataBaseUserRepository(getJdbcTemplate());
        }
        return userRepository;
    }

    private JdbcTemplate getJdbcTemplate() {
        if (jdbcTemplate == null) {
            jdbcTemplate = new JdbcTemplate(getDataSource());
        }
        return jdbcTemplate;
    }

    private PasswordEncoder getPasswordEncoder() {
        return new Md5PasswordEncoder();
    }

    private List<String> getSettings() {
        List<String> settings = new ArrayList<>();
        String path = properties.getProperty("data.base.name");
        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            while (true) {
                String line = reader.readLine();
                if (line == null) break;
                settings.add(line);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return settings;
    }
}

