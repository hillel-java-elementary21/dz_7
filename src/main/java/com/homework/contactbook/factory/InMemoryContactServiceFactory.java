package com.homework.contactbook.factory;

import com.homework.contactbook.model.Contact;
import com.homework.contactbook.model.ContactType;
import com.homework.contactbook.persistance.ContactRepository;
import com.homework.contactbook.persistance.InMemoryContactRepository;
import com.homework.contactbook.service.authservice.AuthService;
import com.homework.contactbook.service.authservice.InMemoryAuthService;
import com.homework.contactbook.service.contactservice.ContactService;
import com.homework.contactbook.utills.validator.Validator;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class InMemoryContactServiceFactory implements ContactServiceFactory {
    private final Validator<Contact> contactValidator;

    @Override
    public ContactService createContactService() {
        return new ContactService(createContactRepository(), contactValidator);
    }

    @Override
    public AuthService createAuthService() {
        return new InMemoryAuthService();
    }

    private ContactRepository createContactRepository() {
        return new InMemoryContactRepository(initialContacts());
    }

    private List<Contact> initialContacts() {
        return new ArrayList<>(List.of(
                new Contact()
                        .setId("f5e7d13d-5eb1-4c8c-9c0a-ad1f04482112")
                        .setName("Ihor")
                        .setSurname("Belyi")
                        .setType(ContactType.PHONE)
                        .setValue("+380639651580"),
                new Contact()
                        .setId("f8e6d13d-5eb1-4c8c-9c0a-ad1f04482117")
                        .setName("Ihor")
                        .setSurname("Tokar")
                        .setType(ContactType.PHONE)
                        .setValue("+380639650580"),
                new Contact()
                        .setId("f8e6d13d-5eb1-4c8c-9c0a-ad1f04482172")
                        .setName("Anatoliy")
                        .setSurname("Dvoriyanov")
                        .setType(ContactType.EMAIL)
                        .setValue("googler@mail.ua"),
                new Contact()
                        .setId("f8e6d13d-5eb1-4c8c-9c0a-ad1f04482112")
                        .setName("Dmitryi")
                        .setSurname("Vorobyov")
                        .setType(ContactType.EMAIL)
                        .setValue("+380tolik@mail.ua"),
                new Contact()
                        .setId("f8e6d13d-5eb1-4c8c-9c0a-ad1f04482812")
                        .setName("Sergey")
                        .setSurname("Kukushkin")
                        .setType(ContactType.EMAIL)
                        .setValue("tost@mail.ua"),
                new Contact()
                        .setId("f8e7d13d-5ea1-4c8c-9c0a-ad1f04487120")
                        .setName("Vladimir")
                        .setSurname("Velikiy")
                        .setType(ContactType.PHONE)
                        .setValue("+380779651581")
        ));

    }
}
