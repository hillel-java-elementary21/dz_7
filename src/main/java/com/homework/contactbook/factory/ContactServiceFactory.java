package com.homework.contactbook.factory;

import com.homework.contactbook.service.authservice.AuthService;
import com.homework.contactbook.service.contactservice.ContactService;

public interface ContactServiceFactory {

    ContactService createContactService();

    AuthService createAuthService();
}