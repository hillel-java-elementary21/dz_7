package com.homework.contactbook.factory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.homework.contactbook.model.Contact;
import com.homework.contactbook.persistance.*;
import com.homework.contactbook.service.authservice.AuthService;
import com.homework.contactbook.service.authservice.InMemoryAuthService;
import com.homework.contactbook.service.contactservice.ContactService;
import com.homework.contactbook.utills.validator.Validator;
import lombok.RequiredArgsConstructor;

import java.util.Properties;

@RequiredArgsConstructor
public class JsonFileContactServiceFactory implements ContactServiceFactory {
    private final Properties properties;
    private final Validator<Contact> contactValidator;

    @Override
    public ContactService createContactService() {
        ContactRepository contactRepository = createContactRepository();
        return new ContactService(contactRepository, contactValidator);
    }

    @Override
    public AuthService createAuthService() {
        return new InMemoryAuthService();
    }

    private ContactRepository createContactRepository() {
        ContactFile contactFile = new JsonContactFile(
                createObjectMapper(),
                properties.getProperty("json.file.name")
        );
        return new CashedContactRepository(
                new FileContactRepository(contactFile)
        );
    }

    private ObjectMapper createObjectMapper() {
        return new ObjectMapper();
    }
}
