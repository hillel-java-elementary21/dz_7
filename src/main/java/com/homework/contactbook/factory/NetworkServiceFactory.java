package com.homework.contactbook.factory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.homework.contactbook.model.Contact;
import com.homework.contactbook.service.authservice.NetworkAuthService;
import com.homework.contactbook.service.contactservice.NetworkContactService;
import com.homework.contactbook.utills.validator.Validator;
import lombok.RequiredArgsConstructor;

import java.net.http.HttpClient;

@RequiredArgsConstructor
public class NetworkServiceFactory {
    private final Validator<Contact> contactValidator;

    private final HttpClient httpClient = createHttpClient();
    private final ObjectMapper json = createObjectMapper();

    public NetworkContactService createContactService() {
        NetworkAuthService authService = createAuthService();
        return new NetworkContactService(
                createObjectMapper(),
                httpClient,
                authService,
                contactValidator
        );
    }

    public NetworkAuthService createAuthService() {
        if (networkAuthService == null) {
            networkAuthService = new NetworkAuthService(json, httpClient);
        }
        return networkAuthService;
    }

    private NetworkAuthService networkAuthService;

    private ObjectMapper createObjectMapper() {
        return new ObjectMapper();
    }

    private HttpClient createHttpClient() {
        return HttpClient.newHttpClient();
    }
}
